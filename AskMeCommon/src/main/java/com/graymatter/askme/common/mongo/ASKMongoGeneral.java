package com.graymatter.askme.common.mongo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.bson.Document;

import com.graymatter.askme.common.exception.ASKPersistenceThrow;
import com.graymatter.askme.common.utility.ASKUtility;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.util.JSON;

public class ASKMongoGeneral {
	private String HOST_NAME;
	private Integer HOST_PORT;
	private String DB_NAME;
	
	private MongoClient mongoClient;
	private MongoDatabase mongoDatabase;
	
	public ASKMongoGeneral(String dbName, String hostName, int hostPort) throws Exception{
		DB_NAME = dbName;
		HOST_NAME = hostName;
		HOST_PORT = hostPort;
		mongoClient = new MongoClient(HOST_NAME, HOST_PORT);
		mongoDatabase = mongoClient.getDatabase(DB_NAME);
	}
	
	protected MongoClient getMongoClient(){return this.mongoClient;}

	protected MongoDatabase getMongoDatabase(){return this.mongoDatabase;}
	
	public void dropDataBase() throws ASKPersistenceThrow{
		mongoDatabase.drop();
	}
	
	public void closeClient() throws ASKPersistenceThrow{
		if(mongoClient!=null) mongoClient.close();
	}
	
	public Iterable<?> find(String name) throws ASKPersistenceThrow {
		MongoCollection<Document> collection = getCollection(name);
		return collection.find();
	}
	
	public Iterable<?> find(String collectionName, BasicDBObject query) throws ASKPersistenceThrow {
		FindIterable<Document> cursor = null;
		try{
			MongoCollection<Document> collection = getMongoDatabase().getCollection(collectionName);
			cursor = collection.find(query);
		} catch(Exception e){
			e.printStackTrace();
		}
		return cursor;
	}
	
	public Iterable<?> findObjectWithMaxId(String collectionName) throws ASKPersistenceThrow{
		FindIterable<Document> cursor = null;
		try{
			MongoCollection<Document> collection = getMongoDatabase().getCollection(collectionName);
			BasicDBObject sort = new BasicDBObject();
			sort.put("_id", -1);
			cursor = collection.find().sort(sort).limit(1);
			
		} catch(Exception e){
			e.printStackTrace();
		}
		return cursor;
	}
	
	public MongoCollection<Document> getCollection(String collectionName) throws ASKPersistenceThrow{
		MongoCollection<Document> collection = mongoDatabase.getCollection(collectionName);
		return collection;
	}
	
	public Document getDocumentFromObject(Object javaObj) throws ASKPersistenceThrow{
		DBObject document = (DBObject) JSON.parse(ASKUtility.ASKJson.GSON.toJson(javaObj));
		return new Document(document.toMap());
	}
	
	public void insert(String collectionName, Document documentToInsert) throws ASKPersistenceThrow {
		try{
			MongoCollection<Document> collection = getMongoDatabase().getCollection(collectionName);
			collection.insertOne(documentToInsert);
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void insert(MongoCollection<Document> collection, Document documentToInsert) throws ASKPersistenceThrow {
		try{
			collection.insertOne(documentToInsert);
		} catch(Exception e){
			e.printStackTrace();
		}
	}
		
	public void createCollection(Collection<?> collection, String name) throws ASKPersistenceThrow{
		MongoCollection<Document> mongoCollection = getCollection(name);
		mongoCollection.drop();
		mongoCollection =  getCollection(name);
		
		List<Document> documents = new ArrayList<Document>();
		for(Object term: collection){
			//DBObject document = (DBObject) JSON.parse(ASKUtility.ASKJson.GSON.toJson(term));
			documents.add(getDocumentFromObject(term));// new Document(document.toMap()));
		}
		mongoCollection.insertMany(documents);
	}
	
	public void createCollection(MongoCollection<?> collection, String name) throws ASKPersistenceThrow{
		MongoCollection<Document> mongoCollection = getCollection(name);
		mongoCollection.drop();
		mongoCollection =  getCollection(name);
		
		List<Document> documents = new ArrayList<Document>();
		FindIterable<Document> cursor = (FindIterable<Document>)collection.find();
		Iterator<Document> iter = cursor.iterator();
		while(iter.hasNext()){
			documents.add(iter.next());
		}
		mongoCollection.insertMany(documents);
	}
	
	public void dropCollection(String name) throws ASKPersistenceThrow{
		MongoCollection<Document> mongoCollection = getCollection(name);
		mongoCollection.drop();
	}
	
	public void update(String collectionName, BasicDBObject documentToSearch, Document documentToUpdate) throws ASKPersistenceThrow {
		try{
			MongoCollection<Document> collection = getMongoDatabase().getCollection(collectionName);
			collection.findOneAndReplace(documentToSearch, documentToUpdate);
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void update(MongoCollection<Document> collection, BasicDBObject documentToSearch, Document documentToUpdate) throws ASKPersistenceThrow {
		try{
			collection.findOneAndReplace(documentToSearch, documentToUpdate);
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public BasicDBObject getFilter(String fieldName, Object fieldValue){
		BasicDBObject filter = new BasicDBObject().append(fieldName, fieldValue);
		if(fieldValue!=null && fieldValue instanceof List) filter = getFilter(fieldName, (List<Object>)fieldValue);
		return filter;
	}

	public BasicDBObject getFilter(String fieldName, Object... fieldValues){
		BasicDBObject filter = new BasicDBObject();
		List<Object> list = new ArrayList<Object>();
		for(int i=0; i<fieldValues.length; i++){
			list.add(fieldValues[i]);
		}
		filter.put(fieldName, new BasicDBObject("$in", list));
		return filter;
	}

	private BasicDBObject getFilter(String fieldName, List<Object> list){
		BasicDBObject filter = new BasicDBObject();
		filter.put(fieldName, new BasicDBObject("$in", list));
		return filter;
	}

}
