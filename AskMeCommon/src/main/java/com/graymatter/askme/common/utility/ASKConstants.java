package com.graymatter.askme.common.utility;

import java.util.Date;

public class ASKConstants {
	
	public static class ASKOutputConstants{
		public static final String FILENAME = 				"extract[@]";
		public static final String ROOT = 					"documents";
		public static final String CONTAINER = 				"document";
		
		public static final String TYPE_STRING = 			"string";
		public static final String TYPE_DATE = 				"date";
		public static final String TYPE_NUMERIC = 			"numeric";
		
		public static final String TAG_REFERENCE = 			"reference";
		public static final String TAG_TITLE =				"title";
		public static final String TAG_CONTENT = 			"content";
		public static final String TAG_SECTION_CONTENT = 	"sectionContent";
		public static final String TAG_SECTION = 			"idSection";
		public static final String TAG_FLAG =				"flag";
		public static final String TAG_FINGERPRINT =		"fingerPrint";
		public static final String TAG_PROVA =				"prova";
	}
	
	public static class ASKParameters{
		public static String FLAG_INSERT	= "I";
		public static String FLAG_UPDATE	= "U";
		public static String FLAG_EQUAL		= "E";
	}
	
	public static class ASKQueryConstant{
		public static String QUERY_OPEN_PARENTHESIS = 		"(";
		public static String QUERY_CLOSE_PARENTHESIS = 		")";
		public static String QUERY_OR =	 					"|";
		public static String QUERY_AND =	 				"&";
		public static String QUERY_NOT =				 	"!";
		public static String QUERY_SINGLE_QUOTE =			"'";

	}
	
	public static class ASKLoggerConstants {
		public static String LOGGER_ACTION =		"action";
		public static String LOGGER_DATE =			"date";
		public static String LOGGER_DESCRIPTION =	"description";
		public static String LOGGER_EXCEPTION =		"exception";
		public static String LOGGER_NAME =			"logNameConstant";
		public static String LOGGER_RESULT = 		"result";
	}

}
