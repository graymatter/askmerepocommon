package com.graymatter.askme.common.utility;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class ASKUtility {
	public static class ASKJson{
		public static Gson GSON = new GsonBuilder().create();
		public static Gson PP_GSON = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();
		public static JsonParser JSON_PARSER = new JsonParser();

		public static JsonElement parseJSON(String json) { 
			return ASKJson.JSON_PARSER.parse(json); 
		}
	}

}
